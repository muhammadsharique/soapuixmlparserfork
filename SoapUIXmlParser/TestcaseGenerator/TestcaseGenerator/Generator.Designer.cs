﻿namespace TestcaseGenerator
{
	partial class Generator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Services = new System.Windows.Forms.RichTextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.Generate = new System.Windows.Forms.Button();
			this.OutputFile = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.ContractName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.FilePath = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// Services
			// 
			this.Services.Location = new System.Drawing.Point(112, 148);
			this.Services.Name = "Services";
			this.Services.Size = new System.Drawing.Size(210, 153);
			this.Services.TabIndex = 27;
			this.Services.Text = "";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(34, 151);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(48, 13);
			this.label4.TabIndex = 26;
			this.label4.Text = "Services";
			// 
			// Generate
			// 
			this.Generate.Location = new System.Drawing.Point(34, 334);
			this.Generate.Name = "Generate";
			this.Generate.Size = new System.Drawing.Size(288, 23);
			this.Generate.TabIndex = 25;
			this.Generate.Text = "Generate";
			this.Generate.UseVisualStyleBackColor = true;
			this.Generate.Click += new System.EventHandler(this.Generate_Click);
			// 
			// OutputFile
			// 
			this.OutputFile.Location = new System.Drawing.Point(112, 110);
			this.OutputFile.Name = "OutputFile";
			this.OutputFile.Size = new System.Drawing.Size(210, 20);
			this.OutputFile.TabIndex = 24;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(34, 113);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(58, 13);
			this.label3.TabIndex = 23;
			this.label3.Text = "Output File";
			// 
			// ContractName
			// 
			this.ContractName.Location = new System.Drawing.Point(112, 39);
			this.ContractName.Name = "ContractName";
			this.ContractName.Size = new System.Drawing.Size(210, 20);
			this.ContractName.TabIndex = 22;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(34, 42);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(78, 13);
			this.label2.TabIndex = 21;
			this.label2.Text = "Contract Name";
			// 
			// FilePath
			// 
			this.FilePath.Location = new System.Drawing.Point(112, 75);
			this.FilePath.Name = "FilePath";
			this.FilePath.Size = new System.Drawing.Size(210, 20);
			this.FilePath.TabIndex = 20;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(34, 78);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(56, 13);
			this.label1.TabIndex = 19;
			this.label1.Text = "Select File";
			// 
			// Generator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(361, 402);
			this.Controls.Add(this.Services);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.Generate);
			this.Controls.Add(this.OutputFile);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.ContractName);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.FilePath);
			this.Controls.Add(this.label1);
			this.Name = "Generator";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.RichTextBox Services;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button Generate;
		private System.Windows.Forms.TextBox OutputFile;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox ContractName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox FilePath;
		private System.Windows.Forms.Label label1;
	}
}

