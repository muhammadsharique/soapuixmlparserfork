﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestcaseGenerator
{
	public partial class Generator : Form
	{
		public Generator()
		{
			InitializeComponent();
		}

		private void Generate_Click(object sender, EventArgs e)
		{
			var data = global::Generator.Generator.GeneratTestcases(ContractName.Text, FilePath.Text, Services.Text, OutputFile.Text);

			new Result(data).ShowDialog();
		}
	}
}
