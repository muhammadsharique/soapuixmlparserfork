﻿namespace TestcaseGenerator
{
	partial class RequestViewer
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.RequestText = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// RequestText
			// 
			this.RequestText.AutoSize = true;
			this.RequestText.Location = new System.Drawing.Point(12, 9);
			this.RequestText.Name = "RequestText";
			this.RequestText.Size = new System.Drawing.Size(68, 13);
			this.RequestText.TabIndex = 1;
			this.RequestText.Text = "RequestText";
			// 
			// RequestViewer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(588, 474);
			this.Controls.Add(this.RequestText);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "RequestViewer";
			this.Text = "RequestViewer";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label RequestText;
	}
}