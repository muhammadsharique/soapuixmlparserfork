﻿namespace TestcaseGenerator
{
	partial class Result
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.Reject = new System.Windows.Forms.TreeView();
			this.Accept = new System.Windows.Forms.TreeView();
			this.Ignore = new System.Windows.Forms.TreeView();
			this.SuspendLayout();
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(17, 18);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(41, 13);
			this.label5.TabIndex = 24;
			this.label5.Text = "Accept";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(303, 18);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(37, 13);
			this.label6.TabIndex = 28;
			this.label6.Text = "Ignore";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(591, 18);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(38, 13);
			this.label7.TabIndex = 29;
			this.label7.Text = "Reject";
			// 
			// Reject
			// 
			this.Reject.Location = new System.Drawing.Point(594, 44);
			this.Reject.Name = "Reject";
			this.Reject.Size = new System.Drawing.Size(272, 485);
			this.Reject.TabIndex = 34;
			this.Reject.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.Reject_NodeMouseClick);
			// 
			// Accept
			// 
			this.Accept.Location = new System.Drawing.Point(20, 44);
			this.Accept.Name = "Accept";
			this.Accept.Size = new System.Drawing.Size(272, 485);
			this.Accept.TabIndex = 35;
			this.Accept.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.Accept_NodeMouseClick);
			// 
			// Ignore
			// 
			this.Ignore.Location = new System.Drawing.Point(306, 44);
			this.Ignore.Name = "Ignore";
			this.Ignore.Size = new System.Drawing.Size(272, 485);
			this.Ignore.TabIndex = 36;
			this.Ignore.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.Ignore_NodeMouseClick);
			// 
			// Result
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(887, 552);
			this.Controls.Add(this.Ignore);
			this.Controls.Add(this.Accept);
			this.Controls.Add(this.Reject);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(903, 591);
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(903, 591);
			this.Name = "Result";
			this.Text = "Result";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TreeView Reject;
		private System.Windows.Forms.TreeView Accept;
		private System.Windows.Forms.TreeView Ignore;
	}
}