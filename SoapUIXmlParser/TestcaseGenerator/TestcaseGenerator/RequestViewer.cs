﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestcaseGenerator
{
	public partial class RequestViewer : Form
	{
		private RequestViewer()
		{
			InitializeComponent();
		}

		public RequestViewer(string request) : this()
		{
			RequestText.Text = request;
		}
	}
}
