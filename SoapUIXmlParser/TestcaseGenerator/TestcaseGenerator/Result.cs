﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestcaseGenerator.Properties;

namespace TestcaseGenerator
{
	public partial class Result : Form
	{
		private readonly IList<IList<KeyValuePair<string, string>>> _data;

		public Result(IList<IList<KeyValuePair<string, string>>> data) 
		{
			_data = data;
			InitializeComponent();
			ShowData();
		}

		void ShowData()
		{
			if (_data.Count > 0)
			{
				ImageList myImageList = new ImageList();
				myImageList.Images.Add(Resources.accept);
				myImageList.Images.Add(Resources.ignore);
				myImageList.Images.Add(Resources.reject);

				if (_data[0].Count > 0)
				{
					foreach (var item in _data[0])
					{
						Accept.ImageList = new ImageList { Images = { Resources.accept } };
						Accept.Nodes.Add(item.Key, item.Key, 0);
					}
				}

				if (_data[1].Count > 0)
				{
					foreach (var item in _data[1])
					{
						Ignore.ImageList = new ImageList { Images = { Resources.ignore } };
						Ignore.Nodes.Add(item.Key, item.Key, 0);
					}
				}

				if (_data[2].Count > 0)
				{
					foreach (var item in _data[2])
					{
						Reject.ImageList = new ImageList { Images = { Resources.reject } };
						Reject.Nodes.Add(item.Key, item.Key, 0);
					}
				}
			}
		}

		private void Accept_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			ShowRequest(e.Node.Text, 0);
		}

		private void Ignore_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			ShowRequest(e.Node.Text, 1);
		}

		private void Reject_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			ShowRequest(e.Node.Text, 2);
		}

		private void ShowRequest(string nodeText, int index)
		{
			var request = _data[index].Single(x => x.Key == nodeText).Value;
			new RequestViewer(request).ShowDialog();
		}
	}
}
