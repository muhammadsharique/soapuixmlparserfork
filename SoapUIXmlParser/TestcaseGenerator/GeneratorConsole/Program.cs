﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			var contractName = "LP-06";
			var filePath = @"E:\Implemented Contract\PAA-01\Performance\TestCase XML\PAA-01.xml";
			var services = "NextDay,SameDay,NextDayDeliveryWindow,SameDayDeliveryWindow,GenericServiceType";
			var outputPath = @"E:\Implemented Contract\PAA-01\Performance\TestCase XML";

			var result = Generator.Generator.GeneratTestcases(contractName, filePath, services, outputPath);
		}
	}
}
