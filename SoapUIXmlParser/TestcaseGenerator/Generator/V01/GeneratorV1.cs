﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Generator.Interface;

namespace Generator.V01
{
    internal class GeneratorV1 : IGenerator
	{
		public IList<IList<KeyValuePair<string, string>>> Generate(string contractName, string filePath, string requiredServices, string outputPath)
		{
			var document = new XmlDocument();
			document.Load(filePath);

			var nsmgr = new XmlNamespaceManager(document.NameTable);
			nsmgr.AddNamespace("con", "http://eviware.com/soapui/config");

			var nodeList = document.SelectNodes("descendant::con:testStep", nsmgr);

			var dictionary = new Dictionary<string, string>();
			var services = requiredServices.Split(',');
			var file = new StringBuilder(requiredServices).Append(Environment.NewLine);
			var testCaseName = string.Empty;

			var incList = new List<KeyValuePair<string, string>>();
			var excList = new List<KeyValuePair<string, string>>();
			var exList = new List<KeyValuePair<string, string>>();

			if (nodeList != null)
			{
				foreach (XmlNode node in nodeList)
				{
					try
					{
						testCaseName = node.Attributes.Item(1).Value;

						var envXmlString = node.ChildNodes[1].ChildNodes[2].ChildNodes[3].LastChild.Value.Replace("\\r", string.Empty).Split(new[] { "<!--Service type" }, StringSplitOptions.None)[0];
						var indexOfUnwantedString = envXmlString.IndexOf("-->", StringComparison.Ordinal);

						if (indexOfUnwantedString < 500)
						{
							envXmlString = envXmlString.Substring(indexOfUnwantedString + 4, envXmlString.Length - (indexOfUnwantedString + 4));
						}

						if (testCaseName.ToLower().Contains("error") || testCaseName.ToLower().Contains("err") || testCaseName.ToLower().Contains("close"))
						{
							excList.Add(new KeyValuePair<string, string>(testCaseName, envXmlString));
							continue;
						}

						incList.Add(new KeyValuePair<string, string>(testCaseName, envXmlString));

						var newXml = new XmlDocument();
						newXml.LoadXml(envXmlString);

						var subNodes = newXml.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes;

						foreach (XmlNode subNode in subNodes)
						{
							try
							{
								if (subNode.ChildNodes.Count > 0)
								{
									var name = subNode.ChildNodes[0].ChildNodes[0].Value.ToLower();

									if (name.Contains("shipment"))
									{
										dictionary.Add(name, subNode.ChildNodes[1].ChildNodes[0].Value);
									}

									if (name.Contains("parcel"))
									{
										dictionary.Add(name.Replace(".", string.Empty), subNode.ChildNodes[1].ChildNodes[0].Value);
									}
								}
							}
							catch
							{
								// ignored
							}
						}

						if (dictionary.Count > 0)
						{
							foreach (var service in services)
							{
								var key1 = $"shipment.{service.ToLower()}";
								var key2 = service.ToLower();

								if (dictionary.ContainsKey(key1))
								{
									file.Append(dictionary[key1]).Append(",");
								}
								else if (dictionary.ContainsKey(key2))
								{
									file.Append(dictionary[key2]).Append(",");
								}
								else
								{
									file.Append(",");
								}
							}

							dictionary.Clear();
							file.Remove(file.Length - 1, 1);
							file.Append(Environment.NewLine);
						}
					}
					catch
					{
						if (incList.Exists(x => x.Key == testCaseName))
						{
							incList.RemoveAll(x => x.Key == testCaseName);
						}

						exList.Add(new KeyValuePair<string, string>(testCaseName, string.Empty));
					}
				}
			}

			File.WriteAllText($"{outputPath}\\{contractName}_Update.txt", file.ToString());

			return new List<IList<KeyValuePair<string, string>>>
			{
				incList,
				excList,
				exList
			};
		}
	}
}
