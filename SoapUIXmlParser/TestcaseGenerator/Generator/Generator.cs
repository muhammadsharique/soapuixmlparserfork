﻿using System;
using System.Collections.Generic;
using System.Text;
using Generator.Interface;
using Generator.V01;

namespace Generator
{
    public static class Generator
    {
	    private static IGenerator _generator;

	    static Generator()
	    {
		    _generator = new GeneratorV1();
		}

	    public static IList<IList<KeyValuePair<string, string>>> GeneratTestcases(string contractName, string filePath, string requiredServices, string outputPath)
	    {
		    return _generator.Generate(contractName, filePath, requiredServices, outputPath);
	    }
	}
}
