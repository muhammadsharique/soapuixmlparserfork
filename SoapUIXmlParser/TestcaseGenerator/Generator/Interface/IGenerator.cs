﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generator.Interface
{
    internal interface IGenerator
    {
	    IList<IList<KeyValuePair<string, string>>> Generate(string contractName, string filePath, string requiredServices, string outputPath);
    }
}
